# Imports dans TELMA CHARTES

## Avant tout 

* dans Telma Chartes : 
    * créer un projet
    * définir une liste de champs 
    * définir des listes propres au projet ou utiliser les listes communes
    * contact @gporte@unistra.fr ou cyril.masset@cnrs-orleans.fr

## Principes généraux

* voir template.xml pour la construction du fichier d'import 
* Racine du document : `<xml>`
* Chaque notice est définie dans un `<record>`
* Chaque notice contient une série d'éléments dans `<elements>` (au pluriel)
* Chaque `<elements>` contient n `<element>` (au singulier)
* Les balises `<element>` décrivent les champs (voir ci-dessous)

------------

## 1. Construction d'une balise `<element>` pour un champ

### 1.1 nom_champ
Nom du champ à donner dans `@type`

### 1.2 nom champs parent
Indique que ce champ ne peut être utilisé seul, mais seulement comme enfant d'un autre champ (voir "dates" et "champs composés")

### 1.3 template_champ
Selon le template, une structuration spécifique est requise.

### 1.4 liste
Nom de la liste correspondant à ce champ.
Des fichiers csv décrivant ces listes sont associés à cette doc.

### 1.4 duplicable
Le champ peut être répété plusieurs fois (oui) ou doit être unique (non).

### 1.5 balisesHTML
Le champ autorise ou non l'utilisation de balises html dans la zone `<value>`

* En général, l'usage de balises HTML dans les champs **n'est pas recommandé**
* Il est cependant **possible** pour les champs de type "text-bloc" (analyse, etc.)
* Les balises autorisées dans ce dernier cas sont : 
	* mise en forme (uniquement `<i>` et `<b>`)
	* liens hypertexte
	* cas particulier des appels de notes : 
		* apparat critique (a) ```<a class="app" href="#app-{lettre_de_la_note}">{lettre_de_la_note}</a>```
	   	* notes historiques (1) ```<a class="note" href="#note-{numéro_de_la_note}">{numéro_de_la_note}</a>```

### 1.6 balises XML
Le champ "transcription" **uniquement** permet l'utilisation de balises XML (TEI).
Leur comportement à l'affichage n'est pas automatique et doit être paramétré (contact gporte@unnistra.fr ou cyril.masset@cnrs-orleans.fr)

Dans les champs paramétrés par défaut on a : 
- `<lb/>` pour les sauts de lignes
- `<hi rend="sup"></hi>` pour les exposants
- `<hi rend="italic"></hi>` pour les italiques

------------


## 2. fichier XML pour l'import 

### 2.1 attributs 

* @id : **ne pas indiquer** => créé lors de l'import
* @status : 1 => non publié ; 2 => publié

### 2.2 balises

#### 2.2.1 notice
Chaque notice est englobée dans une balise `<record>`
```
<record type="acte" status="{1|2}">
	<projectSlug>nom_du_projet</projectSlug>
	<elements>
		<element/>
		<element/>
		...
	</elements>
</record>
```

#### 2.2.2 champs
* L'ensemble des champs est contenu dans une balise `<elements>`
* Chaque champ est contenu dans une balise `<element>` et se compose **au moins** : 
	* d'un `@type` (= nom du champ)
	* d'un `@status` (= statut de publication)
	* d'un sous élément `<value>`
* et **éventuellement** :
	* d'un sous élément `<children>` contenant lui même _n_ sous éléments `<element>`
* Chaque champ répond à un template qui a sa propre logique 

##### 2.2.2.1 champs "text" ou "text-block"
```
<element type="{nom_champ}" status="{1|2}">
	<value>{contenu plein texte}</value>
</element>
```

##### 2.2.2.2 champs "liste"
* Indiquer dans `element` le nom de la liste dans un attribut `@liste`.
* Indiquer dans `value` l'identifiant de la valeur via un `@id`.
* Les identifiants des listes spécifiques au projet peuvent être trouvées dans Telma Chartes > paramètres > Mes projets > {projet} > Listes
* Pour les identifiants des listes communes, voir listes-communes.csv
```
<element type="{nom_champ}" status="{1|2}" liste={nom_de_la_liste}>
	     <value id="{id_value}"/>
</element>
```

#### 2.2.2.3 champs "date"
* Une date est donnée au format suivant : AAAA/MM/JJ ;  AAAA/MM ; AAAA
* Elle est soit **unique** (précise ou approximative), soit sous forme de **période** (après et/ou avant) 

* **date unique précise ou approximative**
```
<element type="date" status="{1|2}">
	<value type="{precise|circa}">{AAAA/MM/JJ}</value>
</element>
```

* **périodes**
	* Il est possible de ne renseigner qu'une seule date :
		* après seul = sans date de fin 
		* avant seul = sans date de début
	* Pas de circa dans le cas de périodes
```
<element type="date" status="{1|2}">
	<value type="date_apres">{AAAA/MM/JJ}</value>
	<value type="date_avant">{AAAA/MM/JJ}</value>
</element>
```

#### 2.2.2.4 champs composés
```
<element type="{nom_champ}" status="{1|2}">
	<value>{contenu plein texte}</value>
	<children>
		<element type="{nom_champ}" status="{1|2}">
			<value>{contenu plein texte}</value>
		</element>
	</children>
</element>
```
* exemple
```
<element type="notes_historiques" status="2">
	<value>Ceci est une note</value>
	<children>
		<element type="numero_note" status="2">
			<value>1/value>
		</element>
	</children>
</element>
```
