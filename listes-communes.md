# oui-non

```
id_valeur;terme
46364;n.c. 	
177;non 	
25;oui
```


# type

```
id_valeur;terme
176;Cartulaire 	
856;Copie 	
46201;Edition 	
92;Lettre 	
23;Original 	
660;Registre 	
1218;Vidimus 	
46364;n.c.
```


# genre

```
id_valeur;terme
1586;Bulle 	
41;Charte 	
46202;Diplôme 	
92;Lettre 	
17;Mandement 	
907;Notice 	
46203;Privilège 	
46364;n.c. 	
18969;pancarte
```


# authenticite

```
id_valeur;terme
46199;douteux 	
46200;faux 	
46364;n.c. 	
6;non suspecté 	
768;suspecté
```


# langue

```
id_valeur;terme
18;Français 	
131014;Latin 	
497;allemand 	
130923;français 	
73557;français et latin 	
93;latin 	
46198;néerlandais
```
